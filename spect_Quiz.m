function varargout = spect_Quiz(varargin)
% SPECT_QUIZ MATLAB code for spect_Quiz.fig
%      SPECT_QUIZ, by itself, creates a new SPECT_QUIZ or raises the existing
%      singleton*.
%
%      H = SPECT_QUIZ returns the handle to a new SPECT_QUIZ or the handle to
%      the existing singleton*.
%
%      SPECT_QUIZ('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPECT_QUIZ.M with the given input arguments.
%
%      SPECT_QUIZ('Property','Value',...) creates a new SPECT_QUIZ or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before spect_Quiz_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to spect_Quiz_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help spect_Quiz

% Last Modified by GUIDE v2.5 14-May-2015 12:51:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @spect_Quiz_OpeningFcn, ...
    'gui_OutputFcn',  @spect_Quiz_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before spect_Quiz is made visible.
function spect_Quiz_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to spect_Quiz (see VARARGIN)

% Choose default command line output for spect_Quiz
handles.output = hObject;

I = imread('FIGURE/ans_1.png');
imshow(I,'parent',handles.axes1);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes spect_Quiz wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = spect_Quiz_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton6.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = guidata(hObject); % 保存しているすべての変数の呼び出し
comcount = handles.comcount; % コマンドウィンドウの行数

% comcount = comcount + 1;
% if comcount > 3
%     clc
%     comcount = 1;
% end
 
% 音声を録音　ー＞　スペクトログラム分析　ー＞　axis2に表示

% Record your voice for 1 seconds.
fs = 8000;
bit = 16;
recObj = audiorecorder(fs,bit,1);
pause(0.4)
disp('話してください！')
recordblocking(recObj, 2);

% Store data in double-precision array.
myRecording = getaudiodata(recObj);

% Plot the waveform.
figure(2);
spectrogram(myRecording,30*fs/1000,28*fs/1000,2^11,fs,'yaxis');
colormap jet

clc
handles.comcount = comcount;
guidata(hObject, handles);



% function togglebutton1_Callback(hObject, eventdata, handles)
% % hObject    handle to togglebutton1 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
%% くいず　スタート！
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clc

% 変数の呼び出し
handles = guidata(hObject);

% 初期値
PUSH = 0;
Qall = 4; % 問題数の総数
count = 1; % 問題番号
comcount = 1;

% 問題文の呼び出し
load('Qtest');
Qtxt = Qtest;

Qnum(1) = randi([1 9]);
Qnum(2) = randi([10 18]);
Qnum(3) = randi([19 24]);
Qnum(4) = randi([25 30]);

% = randperm(Qall,3);
% 問題の順番(ランダムに決定)

% 問 [1] の呼び出し
QUIZ = question1(Qtxt(Qnum(count))); % 選択肢(QUIZ:正しい答えの番号)
I = imread(['FIGURE/f',num2str(Qnum(count)),'.png']); % 画像
imshow(I,'parent',handles.axes1);

% 変数の保存と更新
handles.Qall = Qall;
handles.Qtxt = Qtxt;
handles.Qnum = Qnum;
handles.QUIZ = QUIZ;
handles.PUSH = PUSH;
handles.count = count;
handles.comcount = comcount;
guidata(hObject, handles); % すべての変数の更新



%% --- 答え合わせ & 次の問題
function pushbutton8_Callback(hObject, eventdata, handles)
%% --- 答え合わせ & 次の問題
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% handles = guidata(hObject); % 変数の呼び出し
% guidata(hObject, handles);  % 変数の更新

% 変数の呼び出し
handles = guidata(hObject);
Qall = handles.Qall;
Qtxt = handles.Qtxt;
Qnum = handles.Qnum;
QUIZ = handles.QUIZ;
PUSH = handles.PUSH;
count = handles.count;

%%%%%% 答え合わせ %%%%%%

if QUIZ == PUSH
    I = imread('FIGURE/ans_3maru.png');
    imshow(I,'parent',handles.axes1);
else
    I = imread('FIGURE/ans_4batu.png');
    imshow(I,'parent',handles.axes1);
    %     alpha(0.5)
end

pause(2)

% 問題数が上限に達したら...
if count == Qall
    I = imread('FIGURE/ans_2.png');
    imshow(I,'parent',handles.axes1);
    pause(5)
    I = imread('FIGURE/ans_1.png');
    imshow(I,'parent',handles.axes1);
    return;
end

count = count + 1;

clc

%%%%%% 次の問題 %%%%%%

% 問 [2~] の呼び出し
QUIZ = question1(Qtxt(Qnum(count))); % 選択肢
I = imread(['FIGURE/f',num2str(Qnum(count)),'.png']); % 画像
imshow(I,'parent',handles.axes1);

% 変数の更新
handles.QUIZ = QUIZ;
handles.count = count;
guidata(hObject, handles);




% --- プッシュボタン（１）
function pushbutton9_Callback(hObject, eventdata, handles)

% 変数の呼び出し
handles = guidata(hObject);
handles.PUSH = 1;
guidata(hObject, handles);

% --- プッシュボタン（２）
function pushbutton10_Callback(hObject, eventdata, handles)

% 変数の呼び出し
handles = guidata(hObject);
handles.PUSH = 2;
guidata(hObject, handles);

% --- プッシュボタン（３）
function pushbutton11_Callback(hObject, eventdata, handles)

% 変数の呼び出し
handles = guidata(hObject);
handles.PUSH = 3;
guidata(hObject, handles);

% --- プッシュボタン（４）
function pushbutton12_Callback(hObject, eventdata, handles)

% 変数の呼び出し
handles = guidata(hObject);
handles.PUSH = 4;
guidata(hObject, handles);
